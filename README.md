Ce projet consiste en la mise en place d'un outil de visualisation de la fractale
de Mandelbrot par un client web. Projet réalisé dans le cadre du module de
programmation réseau et concurrente, l'objectif principal est la maitrise de
la programmation réseau avec API java d'I/O non blocants, la sérialisation et la
gestion  de la concurrence avec les Threads.


